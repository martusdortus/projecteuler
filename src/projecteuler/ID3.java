/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600851475143 ?
 */

package projecteuler;

import java.util.Arrays;

/**
 *
 * @author martus
 */
public class ID3 {
    
    /**
     * Generates prime numbers up to n
     * 
     * @param n max value to test
     * @return int[] array of primes
     */
    private static int[] generatePrimes(int n) {
        int[] retVal = new int[0];
        for (int i = 2; i <= n; i++) {
            if(isPrime(i)) {
                retVal = Arrays.copyOf(retVal, retVal.length + 1);
                retVal[retVal.length - 1] = i;
            }
        }
        return retVal;
    }
    
    /**
     * Checks if given number is prime number
     * 
     * @param n number to test
     * @return boolean
     */
    public static boolean isPrime(double n) {
        if(n == 0 || n == 1) {
            return false;
        }
        if(n == 2) {
            return true;
        }
        
        for (int i = 2; i < n; i++) {
            if(n % i == 0) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Find largest factor of given number
     * 
     * @param n long
     * @return long
     */
    public static long findLargestPrimeFactor(long n) {
        long rest = n;
        
        while(! isPrime(rest)) {
            for (int i = 2; i < rest; i++) {
                if( (rest % i) == 0) {
                    rest = rest / i;
                    break;
                }
            }
        }
        return rest;
    }
}